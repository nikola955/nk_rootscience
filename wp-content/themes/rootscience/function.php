<?php
/**
 * Author: Nikola Kokic
 *
 * Rootscience functions and definitions
 *
 * @package Rootscience
 */

 /** Custom settings */
require_once 'library/custom-settings.php';
