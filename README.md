Rootscience is based on Storefront starter theme, which can be found [here](https://docs.woocommerce.com/documentation/themes/storefront/).
## Getting started

To run this project in local you will have to use [sc_homestead_vagrant]

Create the Homestead.yaml file and copy settings from Homestead.yaml.example.

In the terminal, change directory to /Homestead/ and run vagrant up && vagrant ssh.

After that create the new database with name defined in Homestad.yaml file:

mysql -uhomestead -psecret -e "create database database_name;"

After that you will have to load demo dump of the database from your project in to the newly created database in homestead_vagrant:

mysql --verbose -uhomestead -psecret database_name < path_to_mysql_dump.sql

Now you need to run flip from the VM to switch from nginx to apache, you should get the confirmation messages nginx stopped and apache started, reload the page in the browser and the project should be working fine.

Add projects to the host machine's /etc/hosts file with IP address specified in the Homestead.yaml file, eg. 192.168.10.10 rootscience.local

Create `wp-config.php` and `.htaccess` files from samples provided in the root with:

```
cp wp-config-sample.php wp-config.php
cp .htaccess.sample .htaccess
```

Update `wp-config.php` file `DB_NAME`, `DB_USER` and `DB_PASSWORD`
